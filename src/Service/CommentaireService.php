<?php

namespace App\Service;

use App\Entity\BlogPost;
use App\Entity\Commentaire;
use App\Entity\Peinture;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class CommentaireService
{
    private EntityManagerInterface $manager;
    private FlashBagInterface $flash;

    public function __construct(EntityManagerInterface $manager, FlashBagInterface $flash)
    {
        $this->manager = $manager;
        $this->flash = $flash;
    }

    public function persistCommentaire(
        Commentaire $commentaire,
        BlogPost $blogPost = null,
        Peinture $peinture = null
    ): void {
        $commentaire->setIsPublished(false)
                    ->setBlogpost($blogPost)
                    ->setPeinture($peinture)
                    ->setCreatedAt(new \DateTime('now'));

        $this->manager->persist($commentaire);
        $this->manager->flush();
        $this->flash->add('success', 'Votre commentaire est bien envoyé, merci. Il sera publié après validation!');
    }
}
