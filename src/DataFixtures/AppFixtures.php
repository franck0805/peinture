<?php

namespace App\DataFixtures;

use App\Entity\BlogPost;
use App\Entity\Categorie;
use App\Entity\Peinture;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * @codeCoverageIgnore
 */
class AppFixtures extends Fixture
{
    public function __construct(
        private UserPasswordHasherInterface $passwordHasher
    ) {
    }

    public function load(ObjectManager $manager): void
    {

        $faker = Factory::create('fr_FR');
        $user = new User();

        $user->setEmail('user@test.com')
            ->setNom($faker->firstName())
            ->setPrenom($faker->lastName())
            ->setTelephone($faker->phoneNumber())
            ->setAPropos($faker->text())
            ->setInstagram('instagram')
            ->setRoles(['ROLE_PEINTRE']);

        $hashedPassword = $this->passwordHasher->hashPassword(
            $user,
            "password"
        );
        $user->setPassword($hashedPassword);

        $manager->persist($user);

        // Creation de 10 blogPost
        for ($i = 0; $i < 10; $i++) {
            $blogPost = new BlogPost();

            $blogPost->setTitre($faker->words(3, true))
                ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
                ->setContenu($faker->text(350))
                ->setSlug($faker->slug(3))
                ->setUser($user);
            $manager->persist($blogPost);
        }

        // Création d'un blogpost pour les tests
        $blogpost = new BlogPost();

        $blogPost->setTitre('Blogpost test')
                 ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
                 ->setContenu($faker->text(350))
                 ->setSlug('blogpost-test')
                 ->setUser($user);

        for ($k = 0; $k < 5; $k++) {
            $categorie = new Categorie();

            $categorie->setNom($faker->word())
                ->setDescription($faker->words(10, true))
                ->setSlug($faker->slug());

            $manager->persist($categorie);

            for ($j = 0; $j < 2; $j++) {
                $peinture = new Peinture();

                $peinture->setNom($faker->words(3, true))
                    ->setLargeur($faker->randomFloat(2, 20, 60))
                    ->setHauteur($faker->randomFloat(2, 20, 60))
                    ->setEnVente($faker->randomElement([true, false]))
                    ->setDateRealisation($faker->dateTimeBetween('-6 month', 'now'))
                    ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
                    ->setDescription($faker->text())
                    ->setPortfolio($faker->randomElement([true, false]))
                    ->setSlug($faker->slug())
                    ->setFile('watercolor.png')
                    ->addCategorie($categorie)
                    ->setPrix($faker->randomFloat(2, 100, 9999))
                    ->setUser($user);

                $manager->persist($peinture);
            }
        }

        // Categorie de test
        $categorie = new Categorie();

        $categorie->setNom('categorie test')
                  ->setDescription($faker->words(10, true))
                  ->setSlug('categorie-test');

        $manager->persist($categorie);

        //Peinture pour les tests
        $peinture = new Peinture();

        $peinture->setNom('peinture test')
                 ->setLargeur($faker->randomFloat(2, 20, 60))
                 ->setHauteur($faker->randomFloat(2, 20, 60))
                 ->setEnVente($faker->randomElement([true, false]))
                 ->setDateRealisation($faker->dateTimeBetween('-6 month', 'now'))
                 ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
                 ->setDescription($faker->text())
                 ->setPortfolio($faker->randomElement([true, false]))
                 ->setSlug('peinture-test')
                 ->setFile('watercolor.png')
                 ->addCategorie($categorie)
                 ->setPrix($faker->randomFloat(2, 100, 9990))
                 ->setUser($user);

        $manager->persist($peinture);

        $manager->flush();
    }
}
