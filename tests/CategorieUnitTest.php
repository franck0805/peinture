<?php

namespace App\Tests;

use App\Entity\Categorie;
use App\Entity\Peinture;
use PHPUnit\Framework\TestCase;

class CategorieUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $categorie= new Categorie();

        $categorie->setNom("Fongang")
                  ->setDescription("description")
                  ->setSlug("slug");

        $this->assertTrue($categorie->getNom() === 'Fongang');
        $this->assertTrue($categorie->getDescription() === 'description');
        $this->assertTrue($categorie->getSlug() === 'slug');
    }

    public function testIsFalse(): void
    {
        $categorie= new Categorie();

        $categorie->setNom("Fongang")
                  ->setDescription("description")
                  ->setSlug("slug");

        $this->assertFalse($categorie->getNom() === 'false');
        $this->assertFalse($categorie->getDescription() === 'false');
        $this->assertFalse($categorie->getSlug() === 'false');
    }

    public function testIsEmpty(): void
    {
        $categorie= new Categorie();

        $this->assertEmpty($categorie->getNom());
        $this->assertEmpty($categorie->getDescription());
        $this->assertEmpty($categorie->getSlug());
        $this->assertEmpty($categorie->getId());    
    }

    public function testAddGetRemovePeinture()
    {
        $peinture = new Peinture();
        $categorie = new Categorie();

        $this->assertEmpty($categorie->getPeintures());

        $categorie->addPeinture($peinture);
        $this->assertContains($peinture, $categorie->getPeintures());

        $categorie->removePeinture($peinture);
        $this->assertEmpty($categorie->getPeintures());
    }
}
