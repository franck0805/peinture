<?php

namespace App\Tests;

use App\Entity\BlogPost;
use App\Entity\Commentaire;
use App\Entity\User;
use DateTime;
use PHPUnit\Framework\TestCase;

class BlogPostUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $blogPost = new BlogPost();
        $datetime = new DateTime();
        $user = new User();

        $blogPost->setTitre("titre")
             ->setCreatedAt($datetime)
             ->setContenu("contenu")
             ->setSlug("slug")
             ->setUser($user);

        $this->assertTrue($blogPost->getTitre() === 'titre');
        $this->assertTrue($blogPost->getCreatedAt() === $datetime);
        $this->assertTrue($blogPost->getContenu() === 'contenu');
        $this->assertTrue($blogPost->getSlug() === 'slug');
        $this->assertTrue($blogPost->getUser() === $user);
    }

    public function testIsFalse(): void
    {
        $blogPost = new BlogPost();
        $datetime = new DateTime();
        $user = new User();

        $blogPost->setTitre("titre")
             ->setCreatedAt($datetime)
             ->setContenu("contenu")
             ->setSlug("slug")
             ->setUser($user);

        $this->assertFalse($blogPost->getTitre() === 'false');
        $this->assertFalse($blogPost->getCreatedAt() === new DateTime());
        $this->assertFalse($blogPost->getContenu() === 'false');
        $this->assertFalse($blogPost->getSlug() === 'false');
        $this->assertFalse($blogPost->getUser() === new User());
    }

    public function testIsEmpty(): void
    {
        $blogPost = new BlogPost();

        $this->assertEmpty($blogPost->getTitre());
        $this->assertEmpty($blogPost->getCreatedAt());
        $this->assertEmpty($blogPost->getContenu());
        $this->assertEmpty($blogPost->getSlug());
        $this->assertEmpty($blogPost->getUser());
        $this->assertEmpty($blogPost->getId());
    }

    public function testAddGetRemoveCommentaire()
    {
        $blogPost = new BlogPost();
        $commentaire = new Commentaire();

        $this->assertEmpty($blogPost->getCommentaires());

        $blogPost->addCommentaire($commentaire);
        $this->assertContains($commentaire, $blogPost->getCommentaires());

        $blogPost->removeCommentaire($commentaire);
        $this->assertEmpty($blogPost->getCommentaires());
    }
}
