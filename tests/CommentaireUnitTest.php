<?php

namespace App\Tests;

use App\Entity\BlogPost;
use App\Entity\Commentaire;
use App\Entity\Peinture;
use DateTime;
use PHPUnit\Framework\TestCase;

class CommentaireUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $commentaire = new Commentaire();
        $datetime = new DateTime();
        $blogPost = new BlogPost();
        $peinture = new Peinture();

        $commentaire->setAuteur("auteur")
             ->setEmail('email@test.com')
             ->setCreatedAt($datetime)
             ->setContenu("contenu")
             ->setBlogpost($blogPost)
             ->setPeinture($peinture);

        $this->assertTrue($commentaire->getAuteur() === 'auteur');
        $this->assertTrue($commentaire->getEmail() === 'email@test.com');
        $this->assertTrue($commentaire->getCreatedAt() === $datetime);
        $this->assertTrue($commentaire->getContenu() === 'contenu');
        $this->assertTrue($commentaire->getBlogpost() === $blogPost);
        $this->assertTrue($commentaire->getPeinture() === $peinture);
    }

    public function testIsFalse(): void
    {
        $commentaire = new Commentaire();
        $datetime = new DateTime();
        $blogPost = new BlogPost();
        $peinture = new Peinture();

        $commentaire->setAuteur("auteur")
             ->setEmail('email@test.com')
             ->setCreatedAt($datetime)
             ->setContenu("contenu")
             ->setBlogpost($blogPost)
             ->setPeinture($peinture);

        $this->assertFalse($commentaire->getAuteur() === 'false');
        $this->assertFalse($commentaire->getEmail() === 'false@test.com');
        $this->assertFalse($commentaire->getCreatedAt() === new DateTime());
        $this->assertFalse($commentaire->getContenu() === 'false');
        $this->assertFalse($commentaire->getBlogpost() === new BlogPost());
        $this->assertFalse($commentaire->getPeinture() === new Peinture());
    }

    public function testIsEmpty(): void
    {
        $commentaire = new Commentaire();

        $this->assertEmpty($commentaire->getAuteur());
        $this->assertEmpty($commentaire->getEmail());
        $this->assertEmpty($commentaire->getCreatedAt());
        $this->assertEmpty($commentaire->getContenu());
        $this->assertEmpty($commentaire->getBlogpost());
        $this->assertEmpty($commentaire->getPeinture());
        $this->assertEmpty($commentaire->getId());
    }
}
