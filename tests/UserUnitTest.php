<?php

namespace App\Tests;

use App\Entity\BlogPost;
use App\Entity\Peinture;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $user = new User();

        $user->setEmail("franck0805@hotmail.fr")
             ->setPrenom("Franck")
             ->setNom("Fongang")
             ->setPassword("password")
             ->setAPropos("a propos")
             ->setInstagram("instagram")
             ->setTelephone('0123456789')
             ->setRoles(['ROLE_TEST']);

        $this->assertTrue($user->getEmail() === 'franck0805@hotmail.fr');
        $this->assertTrue($user->getUserIdentifier() === 'franck0805@hotmail.fr');
        $this->assertTrue($user->getUsername() === 'franck0805@hotmail.fr');
        $this->assertTrue($user->getPrenom() === 'Franck');
        $this->assertTrue($user->getNom() === 'Fongang');
        $this->assertTrue($user->getPassword() === 'password');
        $this->assertTrue($user->getAPropos() === 'a propos');
        $this->assertTrue($user->getInstagram() === 'instagram');
        $this->assertTrue($user->getTelephone() === '0123456789');
        $this->assertTrue($user->getRoles() === ['ROLE_TEST', 'ROLE_USER']);
    }

    public function testIsFalse(): void
    {
        $user = new User();

        $user->setEmail("franck0805@hotmail.fr")
             ->setPrenom("Franck")
             ->setNom("Fongang")
             ->setPassword("password")
             ->setAPropos("a propos")
             ->setInstagram("instagram")
             ->setTelephone('0123456789');

        $this->assertFalse($user->getEmail() === 'false@test.fr');
        $this->assertFalse($user->getUserIdentifier() === 'false@test.fr');
        $this->assertFalse($user->getUsername() === 'false@test.fr');
        $this->assertFalse($user->getPrenom() === 'false');
        $this->assertFalse($user->getNom() === 'false');
        $this->assertFalse($user->getPassword() === 'false');
        $this->assertFalse($user->getAPropos() === 'false');
        $this->assertFalse($user->getInstagram() === 'false');
        $this->assertFalse($user->getTelephone() === '9876543210');
    }

    public function testIsEmpty(): void
    {
        $user = new User();

        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getUserIdentifier());
        $this->assertEmpty($user->getUsername());
        $this->assertEmpty($user->getPrenom());
        $this->assertEmpty($user->getNom());
        $this->assertEmpty($user->getPassword());
        $this->assertEmpty($user->getAPropos());
        $this->assertEmpty($user->getInstagram());
        $this->assertEmpty($user->getTelephone());
        $this->assertEmpty($user->getId());
    }

    public function testAddGetRemovePeinture()
    {
        $user = new User();
        $peinture = new Peinture();

        $this->assertEmpty($user->getPeintures());

        $user->addPeinture($peinture);
        $this->assertContains($peinture, $user->getPeintures());

        $user->removePeinture($peinture);
        $this->assertEmpty($user->getPeintures());
    }

    public function testAddGetRemoveBlogPost()
    {
        $user = new User();
        $blogpost = new BlogPost();

        $this->assertEmpty($user->getBlogPosts());

        $user->addBlogPost($blogpost);
        $this->assertContains($blogpost, $user->getBlogPosts());

        $user->removeBlogPost($blogpost);
        $this->assertEmpty($user->getBlogPosts());
    }
}
