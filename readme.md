# Peinture

Peinture est un site internet présentant des peintures.

## Environnement de developpement

### Pré-requis

* PHP 7.4
* Composer
* Symfony-cli
* Docker
* Docker-compose
* nodejs et npm

Vous pouvez vérifier les pré-réquis (sauf docker et docker-compose) avec la commande suivante (de la CLI symfony) :

```bash
symfony check:requirements
```

### Lancer l'environnement de développement

```bash
composer install
npm install
npm run build
docker-compose up -d
symfony serve -d
```

### Ajouter des données de test

```bash
symfony console doctrine:fixtures:load
```

### Lancer des tests

```bash
php bin/phpunit --testdox
```

## Production

### Envoie des mails des Contacts
Les mails de prise de contact sont stockés en BDD, pour les envoyer au peintre par mail, il faut mettre en place un cron sur :

``` bash
symfony console app:send-contact
```